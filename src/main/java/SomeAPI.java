import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.net.URL;

/**
 * Created by juice on 21.06.2016.
 */
public class SomeAPI {
    public ArrayList<String> expected = new ArrayList<>();

    public ArrayList<String> sendGet(String URL){
        ArrayList<String> XMLs = new ArrayList<>();
        try {
                URL url = new URL(URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                if (conn.getResponseCode() != 203) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;

                while ((output = br.readLine()) != null) {
                    XMLs.add(output);
                }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return XMLs;
    }

    public List<String> readFileMeth(Path path) {
        List<String> URLs = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] temp;
                temp = line.split("   ");
                URLs.add(temp[0]);
                expected.add(temp[1]);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return URLs;

//        try {
//            byte[] data = Files.readAllBytes(path);
//            String content = new String(data, StandardCharsets.UTF_8);
//            System.out.println(content);
//        }
//        catch (Exception e){
//        }
    }
}
