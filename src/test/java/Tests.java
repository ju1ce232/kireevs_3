import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juice on 21.06.2016.
 */
public class Tests {

    @Test
    public void testURLs(){
        ArrayList<String> listXMLs;
        List<String> listURLs;
        SomeAPI api = new SomeAPI();
        Path path = Paths.get("src\\file.txt");

        listURLs = api.readFileMeth(path);

        boolean flag = false;

        for (int i = 0; i < listURLs.size(); i++) {
            listXMLs = api.sendGet(listURLs.get(i));
              for(String s : listXMLs){
                if(s.contains(api.expected.get(i))){
                    flag = true;
                }
            }
            Assert.assertTrue(flag);
        }

    }
}
